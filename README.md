# Lambda Jakarta Community Assets
A community assets of Lambda Jakarta.

## License
All assets on this repository licensed under the
[CC-BY 4.0 License](https://creativecommons.org/licenses/by/4.0/)
which belong to Lambda Jakarta.

